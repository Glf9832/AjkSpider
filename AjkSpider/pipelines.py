# -*- coding: utf-8 -*-
import time
from AjkSpider.items import AjkSzspiderItem,AjkGzspiderItem,AjkZsspiderItem,AjkHzspiderItem,AjkDgspiderItem,AjkFsspiderItem,AjkZhspiderItem
import MySQLdb
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class AjkspiderPipeline(object):
    def process_item(self, item, spider):

        conn = MySQLdb.connect(
            host='localhost',
            port=3306,
            user='root',
            passwd='123456',
            db='ajkspider',
            charset='utf8')
        cur = conn.cursor()
        # cur.execute()

        if item.__class__ == AjkSzspiderItem:
            if ((item['loupanname'][0].encode('utf8')!= '无')or(item['url'][0].encode('utf8')!= '无')or(item['address'][0].encode('utf8')!='无')or(item['brief'][0].encode('utf8')!='暂无动态')or(item['price'][0].encode('utf8')!='0')):
                cur.execute("insert into t_Sz(loupanname,url,address,brief,price) values(%s,%s,%s,%s,%s)", (
                item['loupanname'][0].encode('utf8'),item['url'][0].encode('utf8'), item['address'][0].encode('utf8'),
                item['brief'][0].encode('utf8'), int(item['price'][0].encode('utf8'))));

            # now = time.strftime('%Y-%m-%d', time.localtime())
            # fileName = u'深圳' + now + '.txt'
            # with open(fileName, 'a') as fp:
            #     fp.write(item['loupanname'][0].encode('utf8') + '\n')
            #     fp.write(item['address'][0].encode('utf8') + '\n')
            #     fp.write(item['brief'][0].encode('utf8') + '\n')
            #     fp.write(item['price'][0].encode('utf8') + '\n\n')
        elif item.__class__ == AjkGzspiderItem:
            if ((item['loupanname'][0].encode('utf8')!= '无')or(item['url'][0].encode('utf8')!= '无')or(item['address'][0].encode('utf8')!='无')or(item['brief'][0].encode('utf8')!='暂无动态')or(item['price'][0].encode('utf8')!='0')):
                cur.execute("insert into t_Gz(loupanname,url,address,brief,price) values(%s,%s,%s,%s,%s)", (
                item['loupanname'][0].encode('utf8'),item['url'][0].encode('utf8'), item['address'][0].encode('utf8'),
                item['brief'][0].encode('utf8'), int(item['price'][0].encode('utf8'))));

            # now = time.strftime('%Y-%m-%d', time.localtime())
            # fileName = u'广州' + now + '.txt'
            # with open(fileName, 'a') as fp:
            #     fp.write(item['loupanname'][0].encode('utf8') + '\n')
            #     fp.write(item['address'][0].encode('utf8') + '\n')
            #     fp.write(item['brief'][0].encode('utf8') + '\n')
            #     fp.write(item['price'][0].encode('utf8') + '\n\n')
        elif item.__class__ == AjkZsspiderItem:
            if ((item['loupanname'][0].encode('utf8')!= '无')or(item['url'][0].encode('utf8')!= '无')or(item['address'][0].encode('utf8')!='无')or(item['brief'][0].encode('utf8')!='暂无动态')or(item['price'][0].encode('utf8')!='0')):
                cur.execute("insert into t_Zs(loupanname,url,address,brief,price) values(%s,%s,%s,%s,%s)", (
                item['loupanname'][0].encode('utf8'),item['url'][0].encode('utf8'), item['address'][0].encode('utf8'),
                item['brief'][0].encode('utf8'), int(item['price'][0].encode('utf8'))));

            # now = time.strftime('%Y-%m-%d', time.localtime())
            # fileName = u'中山' + now + '.txt'
            # with open(fileName, 'a') as fp:
            #     fp.write(item['loupanname'][0].encode('utf8') + '\n')
            #     fp.write(item['address'][0].encode('utf8') + '\n')
            #     fp.write(item['brief'][0].encode('utf8') + '\n')
            #     fp.write(item['price'][0].encode('utf8') + '\n\n')
        elif item.__class__ == AjkHzspiderItem:
            if ((item['loupanname'][0].encode('utf8')!= '无')or(item['url'][0].encode('utf8')!= '无')or(item['address'][0].encode('utf8')!='无')or(item['brief'][0].encode('utf8')!='暂无动态')or(item['price'][0].encode('utf8')!='0')):
                cur.execute("insert into t_Hz(loupanname,url,address,brief,price) values(%s,%s,%s,%s,%s)", (
                item['loupanname'][0].encode('utf8'),item['url'][0].encode('utf8'), item['address'][0].encode('utf8'),
                item['brief'][0].encode('utf8'), int(item['price'][0].encode('utf8'))));

            # now = time.strftime('%Y-%m-%d', time.localtime())
            # fileName = u'惠州' + now + '.txt'
            # with open(fileName, 'a') as fp:
            #     fp.write(item['loupanname'][0].encode('utf8') + '\n')
            #     fp.write(item['address'][0].encode('utf8') + '\n')
            #     fp.write(item['brief'][0].encode('utf8') + '\n')
            #     fp.write(item['price'][0].encode('utf8') + '\n\n')
        elif item.__class__ == AjkDgspiderItem:
            if ((item['loupanname'][0].encode('utf8')!= '无')or(item['url'][0].encode('utf8')!= '无')or(item['address'][0].encode('utf8')!='无')or(item['brief'][0].encode('utf8')!='暂无动态')or(item['price'][0].encode('utf8')!='0')):
                cur.execute("insert into t_Dg(loupanname,url,address,brief,price) values(%s,%s,%s,%s,%s)", (
                item['loupanname'][0].encode('utf8'),item['url'][0].encode('utf8'), item['address'][0].encode('utf8'),
                item['brief'][0].encode('utf8'), int(item['price'][0].encode('utf8'))));

            # now = time.strftime('%Y-%m-%d', time.localtime())
            # fileName = u'东莞' + now + '.txt'
            # with open(fileName, 'a') as fp:
            #     fp.write(item['loupanname'][0].encode('utf8') + '\n')
            #     fp.write(item['address'][0].encode('utf8') + '\n')
            #     fp.write(item['brief'][0].encode('utf8') + '\n')
            #     fp.write(item['price'][0].encode('utf8') + '\n\n')
        elif item.__class__ == AjkFsspiderItem:
            if ((item['loupanname'][0].encode('utf8')!= '无')or(item['url'][0].encode('utf8')!= '无')or(item['address'][0].encode('utf8')!='无')or(item['brief'][0].encode('utf8')!='暂无动态')or(item['price'][0].encode('utf8')!='0')):
                cur.execute("insert into t_Fs(loupanname,url,address,brief,price) values(%s,%s,%s,%s,%s)", (
                item['loupanname'][0].encode('utf8'),item['url'][0].encode('utf8'), item['address'][0].encode('utf8'),
                item['brief'][0].encode('utf8'), int(item['price'][0].encode('utf8'))));

            # now = time.strftime('%Y-%m-%d', time.localtime())
            # fileName = u'佛山' + now + '.txt'
            # with open(fileName, 'a') as fp:
            #     fp.write(item['loupanname'][0].encode('utf8') + '\n')
            #     fp.write(item['address'][0].encode('utf8') + '\n')
            #     fp.write(item['brief'][0].encode('utf8') + '\n')
            #     fp.write(item['price'][0].encode('utf8') + '\n\n')
        elif item.__class__ == AjkZhspiderItem:
            if ((item['loupanname'][0].encode('utf8')!= '无')or(item['url'][0].encode('utf8')!= '无')or(item['address'][0].encode('utf8')!='无')or(item['brief'][0].encode('utf8')!='暂无动态')or(item['price'][0].encode('utf8')!='0')):
                cur.execute("insert into t_Zh(loupanname,url,address,brief,price) values(%s,%s,%s,%s,%s)", (
                item['loupanname'][0].encode('utf8'),item['url'][0].encode('utf8'), item['address'][0].encode('utf8'),
                item['brief'][0].encode('utf8'), int(item['price'][0].encode('utf8'))));

            # now = time.strftime('%Y-%m-%d', time.localtime())
            # fileName = u'珠海' + now + '.txt'
            # with open(fileName, 'a') as fp:
            #     fp.write(item['loupanname'][0].encode('utf8') + '\n')
            #     fp.write(item['address'][0].encode('utf8') + '\n')
            #     fp.write(item['brief'][0].encode('utf8') + '\n')
            #     fp.write(item['price'][0].encode('utf8') + '\n\n')
        cur.close()
        conn.commit()
        conn.close()

        return item


