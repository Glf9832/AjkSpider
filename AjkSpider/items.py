# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class AjkSzspiderItem(scrapy.Item):
    '''深圳'''
    loupanname = scrapy.Field()
    url = scrapy.Field()
    address = scrapy.Field()
    brief = scrapy.Field()
    price = scrapy.Field()

class AjkGzspiderItem(scrapy.Item):
    '''广州'''
    loupanname = scrapy.Field()
    url = scrapy.Field()
    address = scrapy.Field()
    brief = scrapy.Field()
    price = scrapy.Field()

class AjkZsspiderItem(scrapy.Item):
    '''中山'''
    loupanname = scrapy.Field()
    url = scrapy.Field()
    address = scrapy.Field()
    brief = scrapy.Field()
    price = scrapy.Field()

class AjkHzspiderItem(scrapy.Item):
    '''惠州'''
    loupanname = scrapy.Field()
    url = scrapy.Field()
    address = scrapy.Field()
    brief = scrapy.Field()
    price = scrapy.Field()

class AjkDgspiderItem(scrapy.Item):
    '''东莞'''
    loupanname = scrapy.Field()
    url = scrapy.Field()
    address = scrapy.Field()
    brief = scrapy.Field()
    price = scrapy.Field()

class AjkFsspiderItem(scrapy.Item):
    '''佛山'''
    loupanname = scrapy.Field()
    url = scrapy.Field()
    address = scrapy.Field()
    brief = scrapy.Field()
    price = scrapy.Field()

class AjkZhspiderItem(scrapy.Item):
    '''珠海'''
    loupanname = scrapy.Field()
    url = scrapy.Field()
    address = scrapy.Field()
    brief = scrapy.Field()
    price = scrapy.Field()

