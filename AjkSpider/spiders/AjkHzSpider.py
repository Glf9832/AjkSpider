# -*- coding: utf-8 -*-
import scrapy
from AjkSpider.items import AjkHzspiderItem

class AjkhzspiderSpider(scrapy.Spider):
    name = "AjkHzSpider"
    allowed_domains = ["hui.fang.anjuke.com"]
    start_urls = ['http://hui.fang.anjuke.com/']

    def parse(self, response):
        subSelector = response.xpath('//div[@class="item-mod"]')
        # items = []
        for sub in subSelector:
            item = AjkHzspiderItem()
            if sub.xpath('./div[@class="infos"]/div[@class="lp-name"]/h3/a/text()') ==[]:
                item['loupanname'] = [u'无']
            else:
                item['loupanname'] = sub.xpath('./div[@class="infos"]/div[@class="lp-name"]/h3/a/text()').extract()

            if sub.xpath('./div[@class="infos"]/div[@class="lp-name"]/h3/a/@href') == []:
                item['url'] = [u'无']
            else:
                item['url'] = sub.xpath('./div[@class="infos"]/div[@class="lp-name"]/h3/a/@href').extract()

            if sub.xpath('./div[@class="infos"]/p[@class="address"]/a/text()') == []:
                item['address'] = [u'无']
            else:
                item['address'] = sub.xpath('./div[@class="infos"]/p[@class="address"]/a/text()').extract()

            if sub.xpath('./div[@class="infos"]/div[@class="data-brief"]/a/text()') == []:
                item['brief'] = [u'暂无动态']
            else:
                item['brief'] = sub.xpath('./div[@class="infos"]/div[@class="data-brief"]/a/text()').extract()

            if sub.xpath('./div[@class="favor-pos"]/p[@class="price"]/span/text()') == []:
                item['price'] = [u'0']
            else:
                item['price'] = sub.xpath('./div[@class="favor-pos"]/p[@class="price"]/span/text()').extract()

            # items.append(item)
            yield item

        nextpage = response.xpath('//div[@class="list-page"]/div[@class="pagination"]/a[@class="next-page next-link"]/@href')[0].extract()
        yield scrapy.Request(url=nextpage,callback=self.parse)
