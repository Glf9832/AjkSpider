#-*-coding:utf-8-*-

from scrapy import cmdline
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from AjkSpider.spiders import AjkSzSpider
from AjkSpider.spiders import AjkGzSpider
from AjkSpider.spiders import AjkZsSpider
from AjkSpider.spiders import AjkHzSpider
from AjkSpider.spiders import AjkDgSpider
from AjkSpider.spiders import AjkFsSpider
from AjkSpider.spiders import AjkZhSpider

# cmdline.execute("scrapy crawl AjkSzSpider -o AjkSzSpider.csv".split())
# cmdline.execute("scrapy crawl AjkSzSpider -o AjkSzSpider.xml".split())
# cmdline.execute("scrapy crawl AjkSzSpider -o AjkSzSpider.pickle".split())
# cmdline.execute("scrapy crawl AjkSzSpider -o AjkSzSpider.marshal".split())
# cmdline.execute("scrapy crawl AjkSzSpider -o AjkSzSpider.json".split())
# cmdline.execute("scrapy crawl AjkSzSpider -o ftp://user:pass@ftp.example.com/path/to/AjkSzSpider.csv".split())

settings = get_project_settings()
process = CrawlerProcess(settings=settings)
process.crawl(AjkSzSpider.AjkszspiderSpider)
process.crawl(AjkGzSpider.AjkgzspiderSpider)
process.crawl(AjkZsSpider.AjkzsspiderSpider)
process.crawl(AjkHzSpider.AjkhzspiderSpider)
process.crawl(AjkDgSpider.AjkdgspiderSpider)
process.crawl(AjkFsSpider.AjkfsspiderSpider)
process.crawl(AjkZhSpider.AjkzhspiderSpider)

process.start()

